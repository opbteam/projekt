##### Avtor: Tadej Slivnik, Boštjan Gec #####

##### btwfyi: psycopg2 ne dela v python 3.5, dela pa v python 3.4.3  #####


# Music #
Repozitorij vsebuje projekt pri predmetu Osnove Podatkovnih Baz.

Podatke pridobiva s pomočjo pythona (requests) na straneh ki so zapisane spodaj.
Aplikacija pa je narjena v R-u (shiny). Potrebno je imeti naslednje razširitve:

* `shiny`
* `dplyr`
* `RPostgreSQL`
* `ggplot2`

## ER diagram ##
![broken link :(]( baza.png )

## Viri ##
* http://www.allmusic.com/
* https://en.wikipedia.org/wiki/List_of_punk_rock_bands,_0%E2%80%93K
* https://en.wikipedia.org/wiki/List_of_punk_rock_bands,_L%E2%80%93Z
* https://en.wikipedia.org/wiki/List_of_hard_rock_musicians_%28A%E2%80%93M%29
* https://en.wikipedia.org/wiki/List_of_hard_rock_musicians_%28N%E2%80%93Z%29