import Reader
# from DownloadInfo import compareArtistStrings

# compareArtistString primerja 2 stringa na razlicne nacine,
# Vrne True ce sta ista, drugace pa False

# Primer uporabe datoteke reader.py
# mors pa seveda met .txt datoteke ze zdownloadane

nullString = "NULL"

## bool v funkcijah je debugMode (printane)
bool = True

#[imeBenda, origins(seznam), genres(seznam), yearsActive(seznam), members(seznam), pastMembers(seznam), website]
bendiWikiInfo = Reader.readFilesWiki(bool)

#[imebenda, albumAwards(seznam), songAwards(seznam), grammys(seznam)]
bendiAllMusicAwards = Reader.readFilesAllMusicAwards(bool)
# "origin" pa "active" je leps na wikipediji kot na AllMusic.

############### SPREMEMBA

## te informacije zj dobis pri WikiInfo

#old:
# #[imebenda, active, formed, disbanded, genre, styles(seznam), members]
# bendiAllMusicArtists = Reader.readFilesAllMusicSidebar(False)

## Albumi ne bodo imeli stylov pa genre... zlo razlicne stvari so ble v primerjavi z informacijami na wiki...

#[imeBenda, imeAlbuma, linkSlikeAlbuma, releaseDate, casAlbuma, recordingDate]
bendiAllMusicAlbums = Reader.readFilesAllMusicAlbums(bool)

#[imeBenda, ImeAlbuma, #, imePesmi, cas]
bendiAllMusicSongs = Reader.readFilesAllMusicSongs(bool)

