# -*- encoding: utf-8 -*-
import requests
import re
import Reader
import datetime

allmusicLINK = "http://www.allmusic.com"

punkUrls = [
    "https://en.wikipedia.org/wiki/List_of_punk_rock_bands,_0%E2%80%93K",
    "https://en.wikipedia.org/wiki/List_of_punk_rock_bands,_L%E2%80%93Z"
]

rockUrls = [
    "https://en.wikipedia.org/wiki/List_of_hard_rock_musicians_%28A%E2%80%93M%29",
    "https://en.wikipedia.org/wiki/List_of_hard_rock_musicians_%28N%E2%80%93Z%29"
]


separator = "'||'"
separator2 = "'|||'"
nullString = "NULL"

bendiWiki = []
bendiLinks = []
bendiAllMusicSidebar = []
bendiAllMusicAlbums = []
bendiAllMusicSongs = []
imenaBendovKiNimajoLinkovNaAllMusic = []

bendiWikiFile = "bendiWiki.txt"
bendiLinksFile = "bendiLinks.txt"
bendiAllMusicSidebarFile = "bendiAllMusicSidebar.txt"
bendiAllMusicAwardsFile = "bendiAllMusicAwards.txt"
bendiAllMusicAlbumsFile = "bendiAllMusicAlbums.txt"
bendiAllMusicSongsFile = "bendiAllMusicSongs.txt"

## pomozna funkcija za allMusicLinks (za primerjanje imena avtorja)
def splitPo(_a, _niz, _niz2 = " "):
    _a = _a.casefold()
    _niz = _niz.casefold()
    _niz2 = _niz2.casefold()
    _a = "".join(_a.split(_niz))
    _a = "".join(_a.split(_niz2))
    _a = "".join(_a.split(" "))
    return _a


def compareArtistStrings(a, b):
    a = changeSpecChar(a)
    b = changeSpecChar(b)
    if (a.casefold() == b.casefold()
        or "".join(a.split(" ")) == "".join(b.split(" "))
        or splitPo(a, "the") == splitPo(b, "the")
        or splitPo(a, ".") == splitPo(b, ".")
        or splitPo(a, "…") == splitPo(b, "…")
        or splitPo(a, "/") == splitPo(b, "/")
        or splitPo(a, "-") == splitPo(b, "-")
        or splitPo(a, "!") == splitPo(b, "!")
        or splitPo(a, "band") == splitPo(b, "band")
        or splitPo(a, "(band)") == splitPo(b, "(band)")
        or (splitPo(a, "'", "g") == splitPo(b, "'", "g") and ("'" in a or "'" in b))
        or splitPo(a, "'") == splitPo(b, "'")
        or splitPo(a, "and", "&amp;") == splitPo(b, "and", "&amp;")
        or splitPo(a, "and", "&") == splitPo(b, "and", "&")
        or splitPo(a, "(", ")") == splitPo(b, "(", ")")
        or splitPo(a, "4", "four") == splitPo(b, "4", "four")
        or (splitPo(a, "ÿ", "y") == splitPo(b, "ÿ", "y") and ("ÿ" in a or "ÿ" in b))
        or (splitPo(a, "é", "e") == splitPo(b, "é", "e") and ("é" in a or "é" in b))
        or (splitPo(a, "š", "s") == splitPo(b, "š", "s") and ("š" in a or "š" in b))
        or (splitPo(a, "ä", "a") == splitPo(b, "ä", "a") and ("ä" in a or "ä" in b))
        or (splitPo(a, "o", "ø") == splitPo(b, "o", "ø") and ("ø" in a or "ø" in b))
        or (splitPo(a, "o", "ó") == splitPo(b, "o", "ó") and ("ó" in a or "ó" in b))
        or (splitPo(a, "o", "ö") == splitPo(b, "o", "ö") and ("ö" in a or "ö" in b))
        or (splitPo(a, "•", "*") == splitPo(b, "•", "*") and ("•" in a or "•" in b))
        or (splitPo(a, "a", "ă") == splitPo(b, "a", "ă") and ("ă" in a or "ă" in b))
        or (splitPo(a, "s", "sh") == splitPo(b, "s", "sh") and ("sh" in a or "sh" in b))
        or (splitPo(a, "p", "ph") == splitPo(b, "p", "ph") and ("ph" in a or "ph" in b))
        or splitPo(a, "kla", "klau") == splitPo(b, "kla", "klau")
        ):
        return True
    return False


def membersRefactor(members):
    if members is not None:
        # print(members.group(1))
        if members.group(1).find("<br />") != -1:
            members = members.group(1).split("<br />")
        elif members.group(1).find("</li>") != -1:
            members = re.findall(r'<li>(.*?)</li>', members.group(1), re.DOTALL)
        else:
            members = [members.group(1)]

        # print(members)
        members2 = []

        for en, ii in enumerate(members):
            if ii.find("<sup") != -1:
                ii = ii.strip()
                ii = ii[:ii.find("<sup")]
            elif ii.find("<a href=") != -1 or ii.find("<a rel") != -1:
                ii = ii.strip()
                ii = ii[ii.find(">") + 1: ii.find("</a>")]
            elif ii.find("<b>") != -1:
                ii = ""

            if ii.find("(") != -1:
                ii = ii[:ii.find("(")]

            ii = ii.strip().strip('"').strip("'")
            if ii != "":
                members2.append(ii)
        # print(members2)
        return members2
    else:
        return nullString


def changeSpecChar(_a):
    _a = re.sub('[ĀĂĄ]', 'A', _a)
    _a = re.sub('[āăą]', 'a', _a)
    _a = re.sub('[ĆĈĊČ]', 'C', _a)
    _a = re.sub('[ćĉċč]', 'c', _a)
    _a = re.sub('[ĎĐ]', 'D', _a)
    _a = re.sub('[ďđ]', 'd', _a)
    _a = re.sub('[ĒĔĖĘĚ]', 'E', _a)
    _a = re.sub('[ēĕėęě]', 'e', _a)
    _a = re.sub('[ĜĞĠĢ]', 'G', _a)
    _a = re.sub('[ĝğġģ]', 'g', _a)
    _a = re.sub('[ĤĦН]', 'H', _a)
    _a = re.sub('[ĥħ]', 'h', _a)
    _a = re.sub('[ĨĪĬĮİ]', 'I', _a)
    _a = re.sub('[ĩīĭįı]', 'i', _a)
    _a = re.sub('[Ĵ]', 'J', _a)
    _a = re.sub('[ĵ]', 'j', _a)
    _a = re.sub('[Ķ]', 'K', _a)
    _a = re.sub('[ķĸ]', 'k', _a)
    _a = re.sub('[ĹĻĽĿŁ]', 'L', _a)
    _a = re.sub('[ĺļľŀł]', 'l', _a)
    _a = re.sub('[ŃŅŇŊ]', 'N', _a)
    _a = re.sub('[ńņňŉŋ]', 'n', _a)
    _a = re.sub('[ŌŎŐ]', 'O', _a)
    _a = re.sub('[ōŏő]', 'o', _a)
    _a = re.sub('[ŔŖŘ]', 'R', _a)
    _a = re.sub('[ŕŗř]', 'r', _a)
    _a = re.sub('[ŚŜŞŠ]', 'S', _a)
    _a = re.sub('[śŝşš]', 's', _a)
    _a = re.sub('[ŢŤŦ]', 'T', _a)
    _a = re.sub('[ţťŧ]', 't', _a)
    _a = re.sub('[ŨŪŬŮŰŲ]', 'U', _a)
    _a = re.sub('[ũūŭůűų]', 'u', _a)
    _a = re.sub('[Ŵ]', 'W', _a)
    _a = re.sub('[ŵ]', 'w', _a)
    _a = re.sub('[ŶŸ]', 'Y', _a)
    _a = re.sub('[ŷ]', 'y', _a)
    _a = re.sub('[ŹŻŽ]', 'Z', _a)
    _a = re.sub('[źżž]', 'z', _a)
    return _a


def getGoodAuthors(_url1, _url2):
    session = requests.Session()
    seznamWIKI = []
    for i in _url1:
        response = session.get(i)
        imeLink = re.findall(r'<tr>.*?<td><a href="(.*?)" title=".*?">(.*?)</a></td>.*?'
                             r'<td>.*?</td>.*?'
                             r'<td>.*?</td>.*?</tr>',
                             response.text, re.DOTALL)

        for j, h in imeLink:
            if j.find('" class="mw-redirect'):
                j = j.split('" class="mw-redirect')[0]
            seznamWIKI.append([h.strip(), "https://en.wikipedia.org" + j])
            print(h)

    for i in _url2:
        seznamWIKI2 = []
        response = session.get(i)
        imeLink = re.findall(r'<li><a href="(.*?)" title=".*?">(.*?)</a>.*?</li>',
                             response.text)

        for j, h in imeLink:
            if j.find('" class="mw-redirect'):
                j = j.split('" class="mw-redirect')[0]
            if h.casefold().find("article") == -1 \
                    and h.casefold().find("list") == -1 \
                    and h.casefold().find("pages") == -1 \
                    and h.casefold().find("dates") == -1 \
                    and h.casefold().find("musical groups") == -1 \
                    and h.casefold().find("accuracy disputes") == -1:
                seznamWIKI2.append([h.strip(), "https://en.wikipedia.org" + j])

        for h in seznamWIKI2[1:]:
            seznamWIKI.append(h)
            print(h[0])

    nePonoviImena = []
    nePonovi = []
    for h in seznamWIKI:
        if h[0] not in nePonoviImena:
            nePonoviImena.append(h[0])
            nePonovi.append(h)
        else:
            print(h[0] + " je ze v seznamu")

    seznamWIKI = nePonovi

    with open(bendiLinksFile, 'w+', encoding='utf-8') as output:
        for i in seznamWIKI:
            found1 = found2 = False
            response = session.get("http://www.allmusic.com/search/artists/" + i[0],
                                   headers={'User-Agent': 'a user agent'})
            allMusicLink = re.findall(
                r'<div class="info">.*?<div class="name">.*?<a href="(.+?)" data-tooltip=".+?">(.*?)</a>.+?</div>',
                response.text, re.DOTALL)

            for j in allMusicLink:
                if compareArtistStrings(j[1], i[0]):
                    allMusicLink = j
                    found1 = True

            if found1:
                print("found link for: " + i[0])

                response = session.get(i[1])
                origin = re.search(r'<th scope="row">Origin</th>.*?'
                                   r'<td>(.*?)</td>',
                                   response.text, re.DOTALL)

                if origin is not None:
                    found2 = True

                if found2:
                    print("found origin for: " + i[0])
                    output.write("{1}{0}{2}{0}{3}".format(separator, changeSpecChar(i[0]), allMusicLink[0], i[1]) + "\n")
                else:
                    print("no origin on: " + i[1])
            else:
                print("no link for: " + i[0])
                print(allMusicLink)

## gre na wikipedijo in vrne linke bendov za wikipedijo
def wikiInfo(imeInLinki, _integerStop=-1):
    session = requests.Session()
    breaker = 0
    with open(bendiWikiFile, 'w+', encoding='utf-8') as output:
        for i in imeInLinki:
            if breaker >= _integerStop:
                break
            breaker += 1

            response = session.get(i[2])
            origin = re.search(r'<th scope="row">Origin</th>.*?'
                               r'<td>(.*?)</td>',
                               response.text, re.DOTALL)

            print(i[0])

            origin = re.split(r'</a>', origin.group(1))

            origins = []
            for en, orr in enumerate(origin):
                if len(orr) > 0:
                    if orr.find('>') != -1:
                        origins.append(orr[orr.find(">")+1:])
                    else:
                        origins.append(orr.strip().strip(",").strip())

            origin = origins
            print(origin)

            for en, orr in enumerate(origin):
                if orr.find("</span>") != -1:
                    origin[en] = orr.split("</span>")[0]

                orr = origin[en]

                if orr.find("<") != -1 and orr.find(">") != -1:
                    origin[en] = orr[orr.find(">") + 1:]

            origins = []
            for en, orr in enumerate(origin):
                a = orr.split(",")
                if len(a) > 1:
                    for ori in a:
                        origins.append(ori.strip())
                else:
                    origins.append(a[0].strip())

            origin = []

            for en, orr in enumerate(origins):
                if orr != '' and orr != '&#160;' and orr != ')':
                    if orr.find("[1]") == -1 and orr.casefold().find("citation") == -1:
                        origin.append(orr)

            origins = origin
            origin = []

            for orr in origins:
                if orr != '':
                    origin.append(orr)

            print(origin)


            genres = re.search(r'<th scope="row"><a href="/wiki/Music_genre" title="Music genre">Genres</a></th>.*?'
                               r'<td>(.*?)</td>',
                               response.text, re.DOTALL)

            yearsActive = re.search(r'<th scope="row"><span class="nowrap">Years active</span></th>.*?'
                               r'<td>(.*?)</td>',
                               response.text, re.DOTALL)

            website = re.search(r'<th scope="row">Website</th>.*?'
                               r'<td>?.*?(https?://[^<"]+).*?</td>?',
                               response.text, re.DOTALL)

            members = re.search(r'<th scope="row">Members</th>.*?'
                                r'<td>(.*?)</td>?',
                                response.text, re.DOTALL)

            pastMembers = re.search(r'<th scope="row"><span class="nowrap">Past members</span></th>.*?'
                                    r'<td>(.*?)</td>?',
                                    response.text, re.DOTALL)

            members = membersRefactor(members)

            pastMembers = membersRefactor(pastMembers)


            if genres is None:
                genres = nullString
            else:
                genres = genres.group(1)
            if yearsActive is None:
                yearsActive = nullString
            else:
                yearsActive = yearsActive.group(1)
            if website is None:
                website = nullString
            else:
                website = website.group(1)


            if genres.find("<a href=") != -1:
                A = []
                # print(genres)
                b = re.findall('<a href=.*?>([a-zA-Z0-9!"#$%&/()=?*~ˇ^˘°˛`˙´˝¨\-.,_;: ]+?)</a>', genres, re.DOTALL)

                for bb in b:
                    if bb != '':
                        A.append(bb.casefold())
                genres = A

            if genres != nullString:

                if type(genres) == str:
                    if genres.find("/") != -1:
                        genres = genres.split("/")
                    else:
                        genres = [genres]

                for en, j in enumerate(genres):
                    genres[en] = j.strip().replace("-", " ").title()

            print(genres)

            if yearsActive != nullString:
                present = re.search(r'present', yearsActive, re.DOTALL)
                yearsActive = re.findall(r'\d\d\d\d', yearsActive, re.DOTALL)

                if present is not None:
                    yearsActive.append("present")

                yearsActive = [yearsActive[0]] + [yearsActive[-1]]

                print(yearsActive)

            print(members)
            print(pastMembers)
            print(website)
            print("\n")

            output.write("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}".format(separator, i[0], origin, genres, yearsActive, members, pastMembers, website) + "\n")


def allMusicAwards(_integerStop=-1):
    with open(bendiAllMusicAwardsFile, 'w+', encoding='utf-8') as output:
        breaker = 0
        for i in bendiLinks:
            if breaker >= _integerStop:
                break
            breaker += 1

            link = i[1] + "/awards"
            print("\nInformacije na linku: " + link + "\n" + i[0])

            session = requests.Session()
            response = session.get(link, headers={'User-Agent': 'a user agent'})

            albumsHTML = re.findall(r'<section class="billboard-albums">(.*?)</section>', response.text, re.DOTALL)
            songsHTML = re.findall(r'<section class="billboard-singles">(.*?)</section>', response.text, re.DOTALL)
            grammysHTML = re.findall(r'<section class="grammy-awards">(.*?)</section>', response.text, re.DOTALL)

            albumAwards = nullString
            if len(albumsHTML) > 0:
                albumAwards = re.findall(r'<td class="album-title" data-sort-value="(.*?)">',
                                         albumsHTML[0], re.DOTALL)

            songAwards = nullString
            if len(songsHTML) > 0:
                songAwards = re.findall(r'<td class="single" data-sort-value="(.*?)">',
                                         songsHTML[0], re.DOTALL)

            grammys = nullString
            if len(grammysHTML) > 0:
                grammys = re.findall(r'<td class="grammy-title" data-sort-value="(.*?)">',
                                        grammysHTML[0], re.DOTALL)

            if albumAwards != nullString:
                a = "["
                for en, ii in enumerate(albumAwards):
                    if en != 0:
                        a += separator2
                    a += changeSpecChar(ii)
                albumAwards = a + "]"

            if songAwards != nullString:
                a = "["
                for en, ii in enumerate(songAwards):
                    if en != 0:
                        a += separator2
                    a += changeSpecChar(ii)
                songAwards = a + "]"

            if grammys != nullString:
                a = "["
                for en, ii in enumerate(grammys):
                    if en != 0:
                        a += separator2
                    a += changeSpecChar(ii)
                grammys = a + "]"

            writeLine = "{1}{0}{2}{0}{3}{0}{4}".format(separator, i[0], albumAwards, songAwards, grammys)
            print(writeLine)
            print("\nWriting to file...")
            output.write(writeLine + "\n")

## Album: [imeBenda, imeAlbuma, AlbumCoverLink, releaseDate, duration, genreLink, genre, seznam Stylov, recordingDate]
## Song: [imeBenda, imeAlbuma, #, imePesmi, dolzinaPesmi]
def allMusicAlbumsAndSongs(_integerStop=-1):
    print("\nAllMusic albums:")
    breaker = 0
    with open(bendiAllMusicAlbumsFile, 'w+', encoding='utf-8') as output,\
            open(bendiAllMusicSongsFile, 'w+', encoding='utf-8') as output2:
        for h in bendiLinks:
            if breaker >= _integerStop:
                break
            breaker += 1

            link = h[1] + "/discography"
            print("\n---------------\n" + h[0] + ", discography link: " + link)

            session = requests.Session()
            response = session.get(link, headers={'User-Agent': 'a user agent'})

            getAlbumLink = re.findall( r'<td class="cover">.*?<a href="(.*?)".*?', response.text, re.DOTALL)

            albumLinki = []

            for i in getAlbumLink:
                if i[0] == "/":
                    albumLinki.append(allmusicLINK + i)
                else:
                    albumLinki.append(i)

            ## do sm smo dobil linke od albumov
            ## zj gremo pa na vsak link...

            for i in albumLinki:
                print("\nalbum link: " + str(i))
                link = i
                session = requests.Session()
                response = session.get(link, headers={'User-Agent': 'a user agent'})

                albumName = re.findall(
                    r'<hgroup>.*?<h1 class="album-title" itemprop="name">\s*(.*?)\s*</h1>.*?</hgroup>',
                    response.text, re.DOTALL)

                podatki = list()
                podatki.append(h[0]) # appenda avtorja

                try: podatki.append(changeSpecChar(albumName[0]))
                except IndexError: break

                albumImageLink = re.findall(r'<div class="album-contain">(.*?)</div>', response.text, re.DOTALL)
                albumImageLink = re.findall(r'<img src="(.*?)"', albumImageLink[0], re.DOTALL)
                releaseDate = re.findall(r'<div class="release-date">.*?<span>.*?(\d{4})</span>.*?</div>',response.text, re.DOTALL)
                duration = re.findall(r'<div class="duration">.*?<span>(.*?)</span>.*?</div>', response.text, re.DOTALL)
                # genre = re.findall(r'<div class="genre">.*?<div>.*?<a href=".*?">(.*?)</a>', response.text, re.DOTALL)
                # styles = re.findall(r'<div class="styles">.*?<div>\s*(.*?)\s*</div>', response.text, re.DOTALL)
                # styles = re.findall(r'<a href=".*?">(.*?)</a>', "".join(styles), re.DOTALL)
                recordingDate = re.findall(r'<div class="recording-date">.*?<div>.*?(\d{4})</div>', response.text, re.DOTALL)

                try: albumImageLink = albumImageLink[0]
                except IndexError: albumImageLink = nullString

                try: releaseDate = releaseDate[0]
                except IndexError: releaseDate = nullString

                try: duration = duration[0]
                except IndexError: duration = nullString

                # try: genre = genre
                # except IndexError: genre = nullString
                #
                # if len(styles) == 0:
                #     styles = nullString

                try: recordingDate = recordingDate[0]
                except IndexError: recordingDate = nullString

                podatki.append(albumImageLink)
                podatki.append(releaseDate)
                podatki.append(duration)
                # for j in genre:
                #     podatki.append(j)
                # podatki.append(styles)
                podatki.append(recordingDate)

                writeLine = str()
                for enum, j in enumerate(podatki):
                    writeLine += str(j)
                    if enum != len(podatki):
                        writeLine += separator

                print(writeLine)

                songs = re.findall(
                    r'<div class="title" itemprop="name">.*?<a href=".*?" itemprop="url">(.*?)</a>.*?</div>',
                    response.text, re.DOTALL)

                lengths = re.findall(
                    r'<td class="time">\s*(\d{1,2}:\d{2}).*?</td>',
                    response.text, re.DOTALL)

                if len(songs) > 0:
                    print("Number of songs found: " + str(len(songs)))
                    print("Number of lengths found: " + str(len(lengths)))
                    output.write(writeLine + "\n")
                    if len(songs) != len(lengths):
                        print("Songs and Lengths don't match: length = 'NULL'")
                        lengths = []
                        for j in range(len(songs)):
                            lengths.append(nullString)
                    print("Writing to file...")
                    for enum, k in enumerate(songs):
                        writeLine = "{1}{0}{2}{0}{3}{0}{4}{0}{5}".format(separator, h[0], changeSpecChar(albumName[0]), enum+1, changeSpecChar(songs[enum]), lengths[enum])
                        print(writeLine)
                        output2.write(writeLine + "\n")
                else:
                    print("No songs in album...")

###################################################################
## TO JE GLAVNA FUNKCIJA
###########################################################################

# # zacetek
# getGoodAuthors(punkUrls, rockUrls)

while True:
    boolean = str(input("Do you want to redownload data? (y/n)"))
    if boolean == "y":
        boolean = True

        while True:
            linkCheckNUM = input("Number of bands you wish to check on AllMusic: ")
            try:
                linkCheckNUM = int(linkCheckNUM)
                if linkCheckNUM >= 0:
                    break
                else:
                    print("Error: input a number greater or equal to 0")
            except ValueError:
                print("ValueError: input a number")

        break
    elif boolean == "n":
        boolean = False
        break
    else:
        print("Error: unknown input, try again")


if boolean:
    beginningTime = datetime.datetime.now()

    bendiLinks = Reader.readFilesLinks()

    # wikiInfo(bendiLinks, linkCheckNUM)

    bendiWiki = Reader.readFilesWiki()

    # allMusicAwards(linkCheckNUM)

    allMusicAlbumsAndSongs(linkCheckNUM)

    print("\nNumber of bands checked on allMusic: " + str(linkCheckNUM))
    print("Time: " + str(datetime.datetime.now() - beginningTime))