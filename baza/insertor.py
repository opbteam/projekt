
# debug na True zapiše exception v file "vse_ujete_napake.txt", ko se ponovi unos.
debug = True

# [less, full]
debug_awards = [ True, False]

# debug = True
naPrinti_flag = True

import datetime
time0 = datetime.datetime.now()

# import PrimerUporabeReadme
import auth as myauth
import re

import psycopg2, psycopg2.extensions, psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s sumniki
conn = psycopg2.connect(database=myauth.db, host=myauth.host, user=myauth.user, password=myauth.password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogocimo transakcije
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

import Reader
import convertor
#[imebenda, active, formed, disbanded, genre, styles(seznam), members]

# for vrstica in bendiWikiInfo:
#     print( conArtist(vrstica))



artists = convertor.zbrisi_navednice(Reader.readFilesWiki(False))
awards = convertor.zbrisi_navednice(Reader.readFilesAllMusicAwards(False))
albums = convertor.zbrisi_navednice(Reader.readFilesAllMusicAlbums(False))
songs = convertor.zbrisi_navednice(Reader.readFilesAllMusicSongs(False))

# importano iz # from DownloadInfo import compareArtistStrings
#sem kar prepisal, ker cene hoce sped downloadat podatke

# print("".join(' .l ba lja.a lk. jd la.alal.'.split(' ')))
#
# print("".join(['lkj','not']))
# print('jeDro'.casefold() == 'JedrO'.casefold(),'jedro'.casefold())

def splitPo(_a, _niz, _niz2 = " "):
    _a = _a.casefold()
    _niz = _niz.casefold()
    _niz2 = _niz2.casefold()
    _a = "".join(_a.split(_niz))
    _a = "".join(_a.split(_niz2))
    _a = "".join(_a.split(" "))
    return _a

def changeSpecChar(_a):
    _a = re.sub('[ĀĂĄ]', 'A', _a)
    _a = re.sub('[āăą]', 'a', _a)
    _a = re.sub('[ĆĈĊČ]', 'C', _a)
    _a = re.sub('[ćĉċč]', 'c', _a)
    _a = re.sub('[ĎĐ]', 'D', _a)
    _a = re.sub('[ďđ]', 'd', _a)
    _a = re.sub('[ĒĔĖĘĚ]', 'E', _a)
    _a = re.sub('[ēĕėęě]', 'e', _a)
    _a = re.sub('[ĜĞĠĢ]', 'G', _a)
    _a = re.sub('[ĝğġģ]', 'g', _a)
    _a = re.sub('[ĤĦН]', 'H', _a)
    _a = re.sub('[ĥħ]', 'h', _a)
    _a = re.sub('[ĨĪĬĮİ]', 'I', _a)
    _a = re.sub('[ĩīĭįı]', 'i', _a)
    _a = re.sub('[Ĵ]', 'J', _a)
    _a = re.sub('[ĵ]', 'j', _a)
    _a = re.sub('[Ķ]', 'K', _a)
    _a = re.sub('[ķĸ]', 'k', _a)
    _a = re.sub('[ĹĻĽĿŁ]', 'L', _a)
    _a = re.sub('[ĺļľŀł]', 'l', _a)
    _a = re.sub('[ŃŅŇŊ]', 'N', _a)
    _a = re.sub('[ńņňŉŋ]', 'n', _a)
    _a = re.sub('[ŌŎŐ]', 'O', _a)
    _a = re.sub('[ōŏő]', 'o', _a)
    _a = re.sub('[ŔŖŘ]', 'R', _a)
    _a = re.sub('[ŕŗř]', 'r', _a)
    _a = re.sub('[ŚŜŞŠ]', 'S', _a)
    _a = re.sub('[śŝşš]', 's', _a)
    _a = re.sub('[ŢŤŦ]', 'T', _a)
    _a = re.sub('[ţťŧ]', 't', _a)
    _a = re.sub('[ŨŪŬŮŰŲ]', 'U', _a)
    _a = re.sub('[ũūŭůűų]', 'u', _a)
    _a = re.sub('[Ŵ]', 'W', _a)
    _a = re.sub('[ŵ]', 'w', _a)
    _a = re.sub('[ŶŸ]', 'Y', _a)
    _a = re.sub('[ŷ]', 'y', _a)
    _a = re.sub('[ŹŻŽ]', 'Z', _a)
    _a = re.sub('[źżž]', 'z', _a)
    return _a

def compareArtistStrings(a, b):
    a = changeSpecChar(a)
    b = changeSpecChar(b)
    if (a.casefold() == b.casefold()
        or "".join(a.split(" ")) == "".join(b.split(" "))
        or splitPo(a, "the") == splitPo(b, "the")
        or splitPo(a, ".") == splitPo(b, ".")
        or splitPo(a, "…") == splitPo(b, "…")
        or splitPo(a, "/") == splitPo(b, "/")
        or splitPo(a, "-") == splitPo(b, "-")
        or splitPo(a, "!") == splitPo(b, "!")
        or splitPo(a, "band") == splitPo(b, "band")
        or splitPo(a, "(band)") == splitPo(b, "(band)")
        or (splitPo(a, "'", "g") == splitPo(b, "'", "g") and ("'" in a or "'" in b))
        or splitPo(a, "'") == splitPo(b, "'")
        or splitPo(a, "and", "&amp;") == splitPo(b, "and", "&amp;")
        or splitPo(a, "and", "&") == splitPo(b, "and", "&")
        or splitPo(a, "(", ")") == splitPo(b, "(", ")")
        or splitPo(a, "4", "four") == splitPo(b, "4", "four")
        or (splitPo(a, "ÿ", "y") == splitPo(b, "ÿ", "y") and ("ÿ" in a or "ÿ" in b))
        or (splitPo(a, "é", "e") == splitPo(b, "é", "e") and ("é" in a or "é" in b))
        or (splitPo(a, "š", "s") == splitPo(b, "š", "s") and ("š" in a or "š" in b))
        or (splitPo(a, "ä", "a") == splitPo(b, "ä", "a") and ("ä" in a or "ä" in b))
        or (splitPo(a, "o", "ø") == splitPo(b, "o", "ø") and ("ø" in a or "ø" in b))
        or (splitPo(a, "o", "ó") == splitPo(b, "o", "ó") and ("ó" in a or "ó" in b))
        or (splitPo(a, "o", "ö") == splitPo(b, "o", "ö") and ("ö" in a or "ö" in b))
        or (splitPo(a, "•", "*") == splitPo(b, "•", "*") and ("•" in a or "•" in b))
        or (splitPo(a, "a", "ă") == splitPo(b, "a", "ă") and ("ă" in a or "ă" in b))
        or (splitPo(a, "s", "sh") == splitPo(b, "s", "sh") and ("sh" in a or "sh" in b))
        or (splitPo(a, "p", "ph") == splitPo(b, "p", "ph") and ("ph" in a or "ph" in b))
        or splitPo(a, "kla", "klau") == splitPo(b, "kla", "klau")
        or splitPo(splitPo(a, "&", "."), "!", "the") == splitPo(splitPo(b, "&", "."), "!", "the")
        or splitPo(a, "pt. 2", "part two") == splitPo(b, "pt. 2", "part two")
        or splitPo(a, "'", '"') == splitPo(b, "'", '"')
        # js dodal:
        or splitPo(splitPo(splitPo(splitPo(splitPo(splitPo(a, "&"), "."), "!"), "\'"), '\"'), ',') ==
            splitPo(splitPo(splitPo(splitPo(splitPo(splitPo(b, "&"), "."), "!"), "\'"), '\"'), ',')

        # or "the " + a.casefold() == b.casefold() + "!"
        # or a.casefold() + "!" == "the " + b.casefold()

        ):
        return True
    return False

print('-'*5,'end of importing','-'*5,'\n'*3)

def poizvedi(n=-1):

    print('\nKaj je trenutno na bazi! :\n')
    # cur.execute("SELECT * FROM artist ; ")
    #
    # for i in cur:
    #     print(i)
    #     print(i['imebenda'], i['active'])

    def selectStar(table, n=-1, orderby=1):
        k=0
        cur.execute("SELECT * FROM {0} ORDER BY {1}; ".format(table,orderby))
        print('   #### {0} : ####'.format(table))
        for i in cur:
            print(i)
            k+=1
            if k==n:
                break

    ## 1
    # selectStar('artist', n)
    # selectStar('genre', n)
    # selectStar('member', n)
    # selectStar('pastmember', n)
    # ## 2
    # selectStar('ima_genre', n)
    # selectStar('ima_memberja', n)
    # selectStar('ima_pastmemberja', n)
    #
    # # selectStar('album', n, 6)
    # selectStar('album', n, 1)
    # ## 3.0
    # # selectStar('song', n, 4)
    # selectStar('album', n, 1)

    def awards(prvi, table, n, ordedby):
        k=0
        cur.execute("SELECT * FROM {1} WHERE stevilo_nagrad IS NOT NULL ORDER BY {2} DESC; ".format(prvi, table, ordedby))
        print('   #### stevilo_nagrad v {0} : ####'.format(table))
        for i in cur:
            print(i)
            k+=1
            if k == n:
                break
        return 1
    awards('imeBenda', 'artist', n, 6)
    awards('imeAlbuma', 'album', n, 6)
    awards('title', 'song', n, 4)



#     cur.execute("SELECT title, stevilo_nagrad, imeAlbuma, imeBenda FROM song WHERE stevilo_nagrad IS NOT NULL ; ")
#     print('   #### stevilo_nagrad v song : ####'.format(table))
#     for i in cur:
#         print(i)
#
# cur.execute("SELECT imeBenda, stevilo_nagrad, imeAlbuma, title  FROM song WHERE stevilo_nagrad IS NOT NULL ; ")
# print('   #### stevilo_nagrad v song : ####'.format(table))
# for i in cur:
#     print(i)
#
# cur.execute("SELECT imeBenda, stevilo_nagrad, imeAlbuma, title  FROM song WHERE stevilo_nagrad IS NOT NULL ; ")
# print('   #### stevilo_nagrad v song : ####'.format(table))
# for i in cur:
#     print(i)


def pocisti():

    # 3
    for i in ['song']:
        cur.execute("DELETE FROM {0} ; ".format(i))
    # 2
    for i in ['album' ,'ima_genre' ,'ima_memberja' ,'ima_pastmemberja']:
        cur.execute("DELETE FROM {0} ; ".format(i))
    # 1
    for i in [ 'artist' ,'genre' ,'member' ,'pastMember' ]:
        cur.execute("DELETE FROM {0} ; ".format(i))






def intoTableG(stolpci, table, entity, tryex = False):

        vrstica = []

        # stolpci:
        st = []

        for n, i in enumerate(table):
            if i != 'NULL':
                st += [stolpci[n]]
                # print(i)

                vrstica += [i]

        # print(st,vrstica)

        stolpcis = str(tuple(st))
        stolpcis = re.sub('\'', '', stolpcis)  # znak ' vrze ven
        if len(st) == 1:
            stolpcis = re.sub(',', '', stolpcis)  # (ime,) -> (ime)

        vrsticas = str(tuple(vrstica))
        if len(vrstica) == 1:
            vrsticas = re.sub(',', '', vrsticas)  # ('Ac4',) -> ('Ac4')

        # print(stolpcis)
        #
        # print(vrsticas)


        if tryex:

            found0 = re.findall(r'\[[^\[\]]+\]', table[0])

            if len(found0) != 0:
                if table[0] == found0[0]:
                    # if '[' in table[0] and ']' in table[0] and st[0].lower() == 'title'.lower():
                    # cur.execute(
                    #     "DELETE FROM {0} WHERE {1} = '{2}' AND {3} = '{4}' AND {5} = '{6}' "
                    #     ";".format(entity, st[0], vrstica[0], st[-2], vrstica[-2], st[-3], vrstica[-3]))
                    if debug:
                        besedilo1 = ('[ blabla ] || {3} z naslovom {0} iz albuma {1} artista {2} sem'
                              ' dodatno zapisal v file, ker je oblike [ blabla ]  v naslovu'
                              '  \n'.format(table[0], vrstica[-1],
                                                 vrstica[-2], stolpci[0]))
                        file.write(besedilo1)
                else:
                    besedilo1 = ('bla [ bla ] bla || {3} z naslovom {0} iz albuma {1} artista {2} sem'
                          ' dodatno zapisal v file, ker je oblike "bla [ bla ] bla"  v naslovu'
                          ' . \n'.format(table[0], vrstica[-1],
                                          vrstica[-2], stolpci[0]))
                    file.write(besedilo1)

            elif '[' in table[0] or ']' in table[0]:
                besedilo1 = ('] ali [ || {3} z naslovom {0} iz albuma {1} artista {2} sem'
                      ' dodatno zapisal v file, ker  vsebuje ] ali [  v naslovu'
                      ' . \n'.format(table[0], vrstica[-1],
                                          vrstica[-2], stolpci[0]))
                file.write(besedilo1)
            #
            # if len(found0) != 0:
            #     if table[0] == found0[0]:
            #         # if '[' in table[0] and ']' in table[0] and st[0].lower() == 'title'.lower():
            #         # cur.execute(
            #         #     "DELETE FROM {0} WHERE {1} = '{2}' AND {3} = '{4}' AND {5} = '{6}' "
            #         #     ";".format(entity, st[0], vrstica[0], st[-2], vrstica[-2], st[-3], vrstica[-3]))
            #         if debug:
            #             file.write('{3} z naslovom {0} iz albuma {1} artista {2} sem'
            #                        ' dodatno zapisal v file, ker je oblike [ blabla ]  v naslovu'
            #                        '  \n'.format(table[0], vrstica[-1],
            #                                      vrstica[-2], stolpci[0]))
            # elif '[' in table[0] or ']' in table[0] :
            #     file.write('{3} z naslovom {0} iz albuma {1} artista {2} sem'
            #                ' dodatno zapisal v file, ker  vsebuje [ in ]  v naslovu'
            #                ' . \n'.format(table[0], vrstica[-1],
            #                               vrstica[-2], stolpci[0]))



            try:

                cur.execute("INSERT INTO {2} {0} VALUES {1}".format(stolpcis, vrsticas, entity))

            except psycopg2.IntegrityError as z:

                detail = re.findall(r'DETAIL:.(.+)', str(z))
                if ' is not present in table ' in str(z):

                    # detail = re.findall(r'DETAIL:.(.+)', str(z))
                    # print(detail[0])

                    # print(str(z))
                    # file.write(str(z))
                    if debug:
                        besedilo0 = 'is not present || vrstice [[ {0} ]] nebom vstavil v bazo, ker: "{1} " . \n'.format(vrsticas, detail[0])
                        # print(besedilo0)
                        file.write(besedilo0)

                # elif ' ' in str(z):
                # elif ' duplicate key value violates unique constraint ' in str(z):
                elif ' already exists.' in str(z):

                    file.write('PRIMARY || vrstice {0} nebom vstavil v bazo, (ker ne ustreza PRIMARY-KEY zahtevi.) ker: {1} \n'.format(vrsticas, detail[0]))

                    found = re.findall(r'\[[^\[\]]+\]', table[0])

                    if len(found) != 0:
                        if table[0] == found[0]:
                            # if '[' in table[0] and ']' in table[0] and st[0].lower() == 'title'.lower():
                            cur.execute(
                                "DELETE FROM {0} WHERE {1} = '{2}' AND {3} = '{4}' AND {5} = '{6}' "
                                ";".format(entity, st[0], vrstica[0], st[-2], vrstica[-2], st[-3], vrstica[-3]))
                            if debug:
                                file.write('Vse pesmi, z naslovom {0} iz albuma {1} artista {2} sem'
                                           ' dodatno izbrisal iz baze, ker je prislo '
                                           'do "[[Untitled Track]]-kindOf problem" . \n'.format(table[0],
                                                                                                vrstica[-1],
                                                                                                vrstica[-2]))
                else:
                    file.write(str(z)+'\n naj bi zdaj vrglo napako!!!')
                    print(str(z))
                    raise z

            global naPrinti_flag
            naPrinti_flag = 1

        else:
            cur.execute("INSERT INTO {2} {0} VALUES {1}".format(stolpcis, vrsticas, entity))
            # print("INSERT INTO {2} {0} VALUES {1}".format(stolpcis, vrsticas, entity))

        return 1



def addMember(styles, table):

    def vsiMemberji(table):
        s = []
        cur.execute("SELECT * FROM {0}".format(table))
        for i in cur:
            # print(i)
            s += [i[0]]
        return s

    if styles != 'NULL':

        for i in styles:
            if styles == ['']:
                print('tuljenje', styles)
                break
            if i not in vsiMemberji(table):
                # print(i, 'se ni v bazy {0}'.format(table))
                cur.execute("INSERT INTO {0}  VALUES ('{1}')".format(table, i))
    return 1

    # -- 1.2

def bridge(relationName, senior, junior):

    if junior != 'NULL':

        try:
            for element in junior:
                cur.execute( "INSERT INTO {0}  VALUES ( '{1}', '{2}') ".format(relationName, senior, element))
        except psycopg2.IntegrityError as z:

            detail = re.findall(r'DETAIL:.(.+)', str(z))
            if ' is not present in table ' in str(z):

                if debug:
                    # bridge('ima_memberja', table[0], member)
                    besedilo0 = 'bridge({0}, {1}, {2}) || is not present || v tabelo {0} nebom ustavil para ({1}, {2}), ker: "{3} " .' \
                          ' \n'.format(relationName, senior, junior, detail )
                    # print(besedilo0)
                    file.write(besedilo0)

            # elif ' duplicate key value violates unique constraint ' in str(z):
            elif ' already exists.' in str(z):
                file.write(
                    'bridge({0}, {1}, {2}) || PRIMARY-key || v tabelo {0} nebom ustavil para ({1}, {2}), ker: "{3} " .' \
                              ' \n'.format(relationName, senior, junior, detail))
            else:
                file.write(str(z) + '\n naj bi zdaj vrglo napako!!!')
                print(str(z))
                raise z

    return 1

def inserti(a=-1, b=-1, c=-1, d=-1):

    #1 ['album', 'artist_in_genre', 'je_member_od', 'je_pastMember_od']
    #2  ['artist', 'genre', 'member', 'pastMember']
    #3  song

    fname = "vse_ujete_napake.txt"
    global file
    file = open(fname, 'w') #bad, raising error
    file = open(fname, 'w', encoding="utf-8") #important to not turn error.

    def intoArtist(n=-1):

        #start doing shit!!!
        k = 0
        for artist0 in artists:


            line = convertor.conArtist(artist0)
            # [imeBenda, website, origin(seznam), formed_leto(text), disbanded_leto(text), member(seznam),pastMember(seznam), genre(seznam)]
            #   0           1           2               3                   4                   5                  6                7
            stolpci = ['imeBenda', 'website', 'origin', 'formed_leto', 'disbanded_leto']
            table = line[:5]
            member = line[5]
            pastMember = line[6]
            genre = line[7]
            # print(table)
            # print(line)

            # if genre == 'NULL':
            #     print(genre, artistList)

            # print('table: {0} \ngenre: {1}\nstyle: {2}\nmember: {3}'.format(table,genre,style,member))

            #1.1
            intoTableG(stolpci, table, 'artist', True)  # inserta v tabelo artist

            # cur.execute("SELECT * FROM artist WHERE imeBenda = '{0}' ; ".format(table[0]))
            # for i in cur:
            #     print(i)

            addMember(genre, 'genre')  # inserta v tabelo style
            addMember(member, 'member')  # inserta v tabelo member
            addMember(pastMember, 'pastMember')  # inserta v tabelo pastMember

            #1.2

            bridge('ima_genre', table[0], genre)
            bridge('ima_memberja', table[0], member)
            bridge('ima_pastMemberja', table[0], pastMember)

            if k == n:
                break
            k+=1

            # 1  ['artist', 'genre', 'member', 'pastMember']
            # 2  ['artist_in_genre', 'je_member_od', 'je_pastMember_od', 'album']
            # 3  song
        return 1


    def intoAlbum(n=-1):

        k = 0
        for album in albums:



            table = convertor.conAlbum(album)
                       #[imeAlbuma, linkSlikeAlbuma, releaseDate, casAlbuma, recordingDate, imeBenda]
            #   0              1                2          3            4            5
            stolpci = [ 'imeAlbuma', 'cover' , 'release_date', 'duration', 'recording_date', 'imeBenda' ]
            # table = line
            # print(table)

            # cur.execute("SELECT * FROM artist WHERE imeBenda = '{0}' ; ".format(table[-1]))
            # for i in cur:
            #     print(i)

            intoTableG(stolpci, table, 'album', True)

            if k == n:
                break
            k+=1


        # 2  ['album', 'artist_in_genre', 'je_member_od', 'je_pastMember_od']
        # 3  song
        return 1

    def intoSong(n=-1):

        k=0
        for song in songs:

            table = convertor.conSong(song)
            # print(table)
            # [ imePesmi, #, cas, imeBenda, ImeAlbuma]
            #       0     1   2        3         4
            stolpci = [ 'title', 'track_number' , 'length_s', 'imeBenda', 'ImeAlbuma' ]
            # table = line
            intoTableG(stolpci ,table ,'song' ,True)

            if k == n:
                break
            k+=1

        # "title z naslovom [CD-ROM Track] iz albuma Nothing to Prove artista Abrasive Wheels sem doda"
        print('\nvsi songi z cd-rommom [start]:')
        cur.execute("SELECT * FROM song WHERE title = '[CD-ROM Track]' ")
        for i in cur:
            print(i)
        print('vsi songi z cd-rommom [middle]:')
        cur.execute("DELETE FROM song WHERE title = '[CD-ROM Track]' ")
        cur.execute("DELETE FROM song WHERE title = '[CD-Rom Track]' ")
        cur.execute("SELECT * FROM song WHERE title = '[CD-ROM Track]' ")
        for i in cur:
            print(i)
        print('Vsi songi z cd-rommom [end].\n')

        if naPrinti_flag == 1 and debug:
            print( 'program je ujel error! Vse errorje, ki jih najde sem zapisal v file "{0}", baza je vseeno zapolnjena'.format(fname))


        return 1



    def intoAwards(d = [-1, 10**5]):
        print('zornja meja je {0}'.format(d[1]))

        k=0
        for j in awards:

            if d[0] <= k <= d[1]:

                if debug_awards[1]: print(j,'j')

                artist = j[0]

                # if artist == 'Bayside':
                #     debug_awards[0] = True
                #     debug_awards[1] = True
                #     k = 1000



                if debug_awards[0]:
                    cur.execute("SELECT imeBenda FROM artist WHERE imeBenda = '{0}' ".format(artist))
                    if len([i for i in cur]) == 0:
                        wtf = 'Artista v AWARDS ni v tableli arsist!! j = {0}'.format(j)

                        print(wtf)
                        file.write(wtf)


                # artists aka. grammys
                if j[3] != 'NULL':
                    cur.execute(
                        "UPDATE artist SET stevilo_nagrad= {0} WHERE imeBenda = '{1}' ".format(len(j[3]), j[0]))
                    if debug_awards[0]:
                        juhu = "koncno en grammy!\n"
                        print(juhu)
                        file.write(juhu)

                # album
                if j[1] != 'NULL':

                    albums = {}
                    for i in j[1]:
                        if i not in albums:
                            albums[i] = 1
                        else:
                            albums[i] += 1

                    if debug_awards[1]:  print(albums,'albums')

                    for nagrajenec, stNagrad in albums.items():

                        cur.execute("SELECT imeAlbuma FROM album WHERE imeBenda = '{0}' ".format(artist))
                        album = ''

                        # album = nagrajenec # to zbrisi
                        for i in cur:
                            if debug_awards[1]:
                                print(i[0],'i v tabeli album')
                                print(nagrajenec, 'nagrajenec')
                            # if i == nagrajenec:
                            # if str(i[0]).lower() == nagrajenec.lower() :
                            if compareArtistStrings(i[0], nagrajenec):
                                album = i[0]
                                if debug_awards[1]: print(album,'compared \'True\' album == nagrajenec ')
                                break
                            else:
                                if debug_awards[1]: print('nista enaka!')

                        if album == '':
                            if debug_awards[0]:
                                besno2 = 'Obstaja album v Awards, ki ni v tabeli album !!!! album: {0} artist: {1}'.format(nagrajenec, artist)
                                # print(besno2)
                                # file.write(besno2 + '\n')

                        cur.execute(
                            "UPDATE album SET stevilo_nagrad= {0} WHERE imeBenda = '{1}' AND imeAlbuma = '{2}' ".format(
                                stNagrad, artist, album))
                        if debug_awards[1]:
                            print('nagrade v albumu [start')
                            cur.execute("SELECT * FROM album WHERE imeBenda = '{0}' AND imeAlbuma = '{1}' ".format(artist, album))
                            for i in cur:
                                print(i)
                            print('nagrade v albumu end]')

                # songs
                if j[2] != 'NULL':
                    dsongs = {}
                    for i in j[2]:
                        if i not in dsongs:
                            dsongs[i] = 1
                        else:
                            dsongs[i] += 1

                    if debug_awards[1]:
                        print(j[2],'j[2]')
                        print(songs,'songs')

                    for nagrajenec, stNagrad in dsongs.items():

                        if debug_awards[1]:
                            cur.execute("SELECT title FROM song WHERE imeBenda = '{0}' ".format(artist))
                            print([i for i in cur], '[i for i in cur]')
                            print([i for i in cur], '[i for i in cur]')
                        cur.execute("SELECT title FROM song WHERE imeBenda = '{0}' ".format(artist))
                        songi = []
                        for i in cur:
                            # print(i)
                            # if i[0].lower() ==  nagrajenec.lower():
                            if compareArtistStrings(i[0], nagrajenec):
                                songi += [i[0]]
                                if debug_awards[1]: print(i, 'ta je v bazi -> gre v songi')
                            else:
                                if debug_awards[1]: print(i, 'tega ni v bazi -> gre v ERROR!')
                                pass

                        if debug_awards[1]: print(songi,'songi')


                        if songi ==[]:
                            if debug_awards[0]:
                                besno0 = 'Obstaja pesem v Awards, ki ni v tabeli song !!!! song: {0} artist: {1}'.format(nagrajenec, artist)
                                print(besno0)
                                file.write(besno0+'\n')
                            if debug_awards[1]:
                                print('songs:')
                                for songsang in songs:
                                    if songsang[0] == artist:
                                        print(songsang)
                                print('awards:')
                                for songaward in awards:
                                    if songaward[0] == artist:
                                        print(songaward)
                                print(':konec awards')
        #                         print('songs:')
        #                         for songsang in Reader.readFilesAllMusicSongs(False):
        #                             if songsang[0] == artist:
        #                                 print(songsang)
        #                         print('awards:')
        #                         for songaward in Reader.readFilesAllMusicAwards(False):
        #                             if songaward[0] == artist:
        #                                 print(songaward)
        #                         print(':konec awards')
    # # ####

                        else:
                            for i in songi:
                                cur.execute(
                                    "UPDATE song SET stevilo_nagrad= {0} WHERE imeBenda = '{1}' AND title = '{2}' ".format(stNagrad,
                                                                                                                       j[0],i))
                                if debug_awards[1]:
                                    print('nagrade v song [start')
                                    cur.execute(
                                        "SELECT * FROM song WHERE imeBenda = '{0}' AND title = '{1}' ".format(artist,
                                                                                                                   i))
                                    for p in cur:
                                        print(p)
                                    print('nagrade v song end]')

                                if debug_awards[1]:
                                    cur.execute("SELECT title FROM song WHERE title =  '{0}' ".format(i))
                                    if len([p for p in cur]) == 0:
                                        besno = "Te pesmi ni v tabeli song: NEKAJ JE FULL NAROBE!!!!!"
                                        print(besno)
                                        file.write(besno)


                        # done, ker nevemo album od pesmi.
            if debug_awards[1]: print('krog k = ',k)
            # if k == d:
            #     break
            k+=1

        return 'intoAwards'

    def clean_awards(default='DEFAULT'):
        cur.execute("UPDATE artist SET stevilo_nagrad= {0} ".format(default))
        cur.execute("UPDATE album SET stevilo_nagrad= {0} ".format(default))
        cur.execute("UPDATE song SET stevilo_nagrad= {0} ".format(default))
        return 'clean_awards'


# casi:
# artistsTime: 0:01:07.446352
# AlbumsTime: 0:00:12.259709
# SongsTime: 0:01:44.562120
# Overall: 0:08:01.116158


    intoArtist(a)             # intoArtist(a)
    artistsTime = datetime.datetime.now() - time0
    print('artistsTime: {0} '.format(artistsTime))
    print('\nProgram se izvaja ... please wait ... \n')
    intoAlbum(b)                  # intoAlbum(b)
    AlbumsTime = datetime.datetime.now() - time0
    print('AlbumsTime: {0} '.format(AlbumsTime-artistsTime))
    print('\nProgram se izvaja ... please wait ... \n')
    intoSong(c)                   # intoSong(c)
    SongsTime = datetime.datetime.now() - time0
    print('SongsTime: {0} '.format(SongsTime-AlbumsTime-artistsTime))
    clean_awards(0)
    intoAwards()                   # intoAwards(d)
    AwardsTime = datetime.datetime.now() - time0
    print('AwardsTime: {0} '.format(AwardsTime-SongsTime-AlbumsTime-artistsTime))
    print("\nNumber of bands checked on allMusic: Unknown")
    print("Overall: " + str(datetime.datetime.now() - time0))

    file.close()
    return 1



print('Program se izvaja ... please wait ... \n')



# print('\nKONEC programa ... NULL psycopg2 izpise kot None')

# print('AWARDS ne dela prav.!!')


print('nov zacetek')

pocisti()
#
# inserti(-1,-1,-1,[-1,1000])
inserti(-1,-1,-1,[-1,220])
poizvedi(10)

def ai():
    fileawput = open('fileawput.txt', 'w', encoding='utf-8')
    fileawards = open('bendiAllMusicAwards.txt')
    awds = fileawards.read()
    fileawards.close()


    print('poizvedba')
    cur.execute("Select * from album ")
    for i in cur:
        # print(i)
        found = re.findall(r'...............{0}............'.format(i[0].casefold()),awds.casefold(), re.DOTALL)
        # print('baza: ',  i['stevilo_nagrad'],'re:', len(found))
        # if i['stevilo_nagrad'] not in [len(found), None]:
        if i['stevilo_nagrad'] != len(found) and not  (i['stevilo_nagrad']==None and len(found) ==0):


            besedilo = 'baza: {0} re: {1} pri albumu: {2} in artistu: {3} re.found: {4}\n'.format(i['stevilo_nagrad'],  len(found), i[0], i[-1], found)
            fileawput.write(str(i) + '\n'+ besedilo)
            print(str(i) + '\n'+ besedilo)
    fileawput.close()

    print('konec')
    return 1
# ai()


#note:

#albume ne file.writa in ne print(a) ,ceprav je debug_awards na True
# print(compareArtistStrings('The Tipping Point', 'The Tipping Point')) problem