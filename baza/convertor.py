import Reader
import re


bool = True
# bendiWikiInfo = Reader.readFilesWiki(bool)
# bendiAllMusicAwards = Reader.readFilesAllMusicAwards(bool)
# bendiAllMusicAlbums = Reader.readFilesAllMusicAlbums(bool)
# bendiAllMusicSongs = Reader.readFilesAllMusicSongs(bool)


def zbrisi_navednice(bendiAllMusic):
    artists = []

    for vrstica in bendiAllMusic:
        zapisi = []
        for atribut in vrstica:
            if  type( atribut ) == type('245') :
                zapisi += [re.sub('\'', '\"', atribut )]
                # print( zapisi )

            elif type( atribut ) == type(['1','2','3']) :
                styli = []
                for stil in atribut :
                    styli += [re.sub('\'', '\"', stil )]
                    # print(styli)
                zapisi += [styli]

            else: print(' element ni string ali seznam !!!!!!!!!!!!!!!!')

        artists += [zapisi]

    return artists

# artists = zbrisi_navednice(bendiAllMusicArtists)
# albums = zbrisi_navednice(bendiAllMusicAlbums)
# songs = zbrisi_navednice(bendiAllMusicSongs)

# for i in artists:
#     print(i)
#
# for i in albums:
#     print(i)
#
# for i in bendiAllMusicSongs:
#     print(i)

# -------------------- zacetek -----------------------------

# Primer uporabe datoteke reader.py
# mors pa seveda met .txt datoteke ze zdownloadane

nullString = "NULL"

## bool v funkcijah je debugMode (printane)
bool = False
# bool = True

# -----------------------------Artist-----------------------------
## NOVO

# len(origin) nikoli ni 0, lahko je od 1-4
# origin se da lepo v bazo -> vedno je drzava, poj pa zmeri mn...
# len(yearsActive) = 2, zacetek in konec (konec je lahko: leto ali niz "present")

## Konec BLABLA


# [imeBenda, origins(seznam), genres(seznam), yearsActive(seznam), members(seznam), pastMembers(seznam), website]
#
# change to [imeBenda, website, origin(seznam), yearsActive_formed(string), yearsActive_disbended(string), members(seznam), pastMembers(seznam), genres(seznam)]
# on baza: [imeBenda, website, origin(seznam), formed_leto(text), disbanded_leto(text), member(seznam), pastMember(seznam), genre(seznam)]

# bendiWikiInfo = Reader.readFilesWiki(False)
# # bendiWikiInfo = Reader.readFilesWiki(bool)
# bendiWikiInfo = zbrisi_navednice(bendiWikiInfo)

def conArtist(bendiWikiInfo_vrstica):

    # # origin ideja
    # a = ['bla','je','sla','po','Gobe']
    #
    # # a.insert(3,'hitro')
    #
    # s = ''
    # for i in a:
    #     s = i+s
    # print(s,'s')
    #
    # # a.reverse()
    # print(a)
    #
    #
    # a = [1,2,34,5,6,6]
    # b=[]
    # for i in a:
    #     b = [i] + b
    #     print(i,b)
    #
    # b = [ a[len(a) - 1 - i] for i in range(0, len(a))]
    #
    # print(a,b)
    #
    # for i in range(0, len(a)):
    #
    #     b+= [a[len(a) - 1 - i]]
    #     print(i,a[len(a) - 1 - i])

    # ##origin:
    # ideja1: origin = [ bendiWikiInfo[1][len(bendiWikiInfo[1]) - 1 - i] for i in range(0, len(bendiWikiInfo[1]))]
    vrstica = bendiWikiInfo_vrstica

    origin = ''
    c=0
    for i in vrstica[1]:
        if c!=0:
            origin = i +', '+ origin
        else:
            origin = i + origin
        c+=1
    if vrstica[3] == ['UL']: #vrstica[3] == 'NULL' ALI 'UL'
        vrstica[3] = ['NULL','NULL']
    # print(vrstica[3])
    artist = [vrstica[0] ,vrstica[6] ,origin ,vrstica[3][0] ,vrstica[3][1] , vrstica[4], vrstica[5], vrstica[2]]
    # 7 len, 0-6

    return artist
    # return 'null'
# [
# 'Blackbird Raum',
# ['Santa Cruz', 'CA'],
# ['anarcho-punk', 'acoustic', 'folk-punk', 'blues', 'traditional folk music', 'ragtime', 'country'],
# ['UL'],
# ['C.P.N.', 'Amelia', 'Zack Religious', 'David', 'Allen Degenerate'],
# ['<p>Roberto Miguel', 'Mars', '</p>\\nJillian Edward Chaplin'],
# 'http://silversprocket.gostorego.com/blackbirdraum.html?limit=all'
# ]


# for vrstica in bendiWikiInfo:
#     print( conArtist(vrstica))


# -----------Nagrade--------------- #not Done



# (grammy je tut za pesem)
# v bazo se da samo stevilo nagrad seprav: vsaka pesem ma lahko nagrado...
# za album se pa lahko nardi da pobere vse nagrade od vseh pesmi (pod tem albumom) (to se lahko poj v sql nardi)
#[imebenda, albumAwards(seznam), songAwards(seznam), grammys(seznam)]
# bendiAllMusicAwards = Reader.readFilesAllMusicAwards(False)
# bendiAllMusicAwards = Reader.readFilesAllMusicAwards(bool)

def seznamMake(awards):
    pass
    # album ima nagrado -> NE dobi vsaka song v tem albumu dodatno nagrado.
    # song ima nagrado -> NE dobi album, ki vsebuje to pesem še dodatno nagrado.

    # plan:
    # into artist
    # 'into album'
    # 'into song'
    # for j in awards:
    #     prestej nagrade s pomocjo slovarja.
    #     for s in slovar:
    #         cur.execute()

        # for i in j[2]:
        #     if i not in artists:
        #         artists[i] = 0
        #     else:
        #         artists[i] += 1




# --------------Albumi--------------------------

# summary: nic dela (1 shift to right)

# --           #[imeBenda, imeAlbuma, linkSlikeAlbuma, releaseDate, casAlbuma, recordingDate]
# --change to: #[ imeAlbuma, linkSlikeAlbuma , releaseDate, casAlbuma, recordingDate, imeBenda ]
# --in baza:   #[ imeAlbuma, cover , release_date, duration, recording_date, imeBenda ]

# bendiAllMusicAlbums = Reader.readFilesAllMusicAlbums(False)
# # bendiAllMusicAlbums = Reader.readFilesAllMusicAlbums(bool)
# bendiAllMusicAlbums = zbrisi_navednice(bendiAllMusicAlbums )

def conAlbum(bendiAllMusicAlbums_vrstica):

    vrstica = bendiAllMusicAlbums_vrstica

    album = vrstica[1:] + [ vrstica[0] ]

    return album
#
# for vrstica in bendiAllMusicAlbums:
#     print( conAlbum(vrstica))
#     pass




# ---------------Songs --------------------------

#easy peasy

# --            #[imeBenda, ImeAlbuma, #, imePesmi, cas]
# -- change to: #[ imePesmi, #, cas, imeBenda, ImeAlbuma]
# -- on baza: #[ title, track_number , length_s, imeBenda, ImeAlbuma]


# bendiAllMusicSongs = Reader.readFilesAllMusicSongs(False)
# # bendiAllMusicSongs = Reader.readFilesAllMusicSongs(bool)
# bendiAllMusicSongs = zbrisi_navednice(bendiAllMusicSongs)

def conSong(bendiAllMusicSongs_vrstica):

    vrstica = bendiAllMusicSongs_vrstica
    #5 : 0-4
    song = [vrstica[3]] + [vrstica[2]] + [vrstica[-1]] + vrstica[:2]

    return song

# for vrstica in bendiAllMusicSongs:
#     print( conSong(vrstica))
#     pass
