#!/usr/bin/env python
#-*- coding: utf-8 -*-

import auth as myauth
import psycopg2, psycopg2.extensions, psycopg2.extras

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s sumniki
# conn = psycopg2.connect(database='banka', host='baza.fmf.uni-lj.si', user='student', password='telebajsek')
conn = psycopg2.connect(database=myauth.db, host=myauth.host, user=myauth.user, password=myauth.password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogocimo transakcije
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


def creating():
    createfname ='sql-create'
    file = open(createfname,'r')
    ukaz = file.read()
    file.close()

    cur.execute(ukaz)    #ta je ta pomemben

    #cur.execute( "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {0}".format(myauth.couser) )
    return 1


def preveri():
    cur.execute("SELECT * FROM artist ; ")

    for i in cur:
        print(i)
        print(i['imebenda'], i['website'])


def destructor():
    dropfname ='sql-drop'
    file = open(dropfname,'r')
    ukaz = file.read()
    file.close()

    cur.execute(ukaz)
    return 1


try:
    creating()
except psycopg2.ProgrammingError:
    destructor()
    creating()

preveri()

print('done!')

