
import auth as myauth
import re

import psycopg2, psycopg2.extensions, psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s sumniki
conn = psycopg2.connect(database=myauth.db, host=myauth.host, user=myauth.user, password=myauth.password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogocimo transakcije
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


cur.execute("SELECT imeBenda from artist LEFT JOIN album USING (imeBenda) "
                         "WHERE imeAlbuma IS NULL "
                         "ORDER BY imeBenda")

osumljenci = [i for i in cur  ]
print(osumljenci)
for i in osumljenci:
    cur.execute("DELETE FROM ima_memberja WHERE artist = '{0}'".format(i[0]))
    cur.execute("DELETE FROM ima_pastMemberja WHERE artist = '{0}'".format(i[0]))
    cur.execute("DELETE FROM ima_genre WHERE artist = '{0}'".format(i[0]))

    cur.execute("DELETE FROM artist WHERE imeBenda = '{0}'".format(i[0]))
    print(i[0])

def preveri():
    k=0
    cur.execute("SELECT * from artist")
    artisti = [i for i in cur]
    for i in artisti:
        # print(i)
        cur.execute("SELECT * FROM album WHERE imeBenda = '{0}'".format(i[0]))
        if len([j for j in cur]) == 0:
            print('JOIN ne dela! Obstaja artist {0}, ki nima nobenega albuma!'.format(j))
        elif k<10:
            print(k)
            k+=1
preveri()