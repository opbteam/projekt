separator = "'||'"
separator2 = "'|||'"
nullString = "NULL"

bendiWikiFile = "bendiWiki.txt"
bendiLinksFile = "bendiLinks.txt"
bendiAllMusicSidebarFile = "bendiAllMusicSidebar.txt"
bendiAllMusicAwardsFile = "bendiAllMusicAwards.txt"
bendiAllMusicAlbumsFile = "bendiAllMusicAlbums.txt"
bendiAllMusicSongsFile = "bendiAllMusicSongs.txt"

#[imebenda, origin, active]
#[wkiLink]

def str2list(a):
    if type(a) is not str:
        print("input is not of type str")
        return

    a = a[1:-1].split(",")
    for i, j in enumerate(a):
        j = j.strip().strip("'").strip()
        a[i] = j

    return a

#[imeBenda, origins(seznam), genres(seznam), yearsActive(seznam), website]
def readFilesWiki(debug = False):
    imeSez = []
    print("\nImporting Wiki names and links...")
    with open(bendiWikiFile, 'r', encoding='utf-8') as readFrom:
        for line in readFrom:
            ime = line.split(separator)[0].strip()

            origin = line.split(separator)[1].strip()
            origin = str2list(origin)

            genre = line.split(separator)[2].strip()
            genre = str2list(genre)

            years = line.split(separator)[3].strip()
            years = str2list(years)

            members = line.split(separator)[4].strip()
            members = str2list(members)

            pastMembers = line.split(separator)[5].strip()
            pastMembers = str2list(pastMembers)

            website = line.split(separator)[6].strip()

            imeSez.append([ime, origin , genre, years, members, pastMembers, website])

    if debug:
        print("\nWiki info: ")
        for i in range(len(imeSez)):
            print("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}".format(", ", imeSez[i][0], imeSez[i][1], imeSez[i][2], imeSez[i][3], imeSez[i][4], imeSez[i][5], imeSez[i][6]))

        print("\nstevilo: " + str(len(imeSez)))
    print("Importing done!")

    return imeSez

#[imeBenda, AllMusicLink]
def readFilesLinks(debug=False):
    print("\nImporting names and links...")
    seznam = []
    with open(bendiLinksFile, 'r', encoding='utf-8') as readFrom:
        for line in readFrom:
            ime = line.split(separator)[0].strip()
            allMusicLink = line.split(separator)[1].strip()
            wikiLink = line.split(separator)[2].strip()
            seznam.append([ime, allMusicLink, wikiLink])

    if debug:
        print("\nImena in linki:")
        for i in seznam:
            print(i)

    print("Importing done!")

    return seznam

#[imebenda, albumAwards(seznam), songAwards(seznam), grammys(seznam)]
def readFilesAllMusicAwards(debug=False):
    print("\nImporting Allmusic Awards info...")
    seznam = []
    with open(bendiAllMusicAwardsFile, 'r', encoding='utf-8') as readFrom:
        for pos, line in enumerate(readFrom):
            # print("\n")
            line = line.split(separator)

            seznam.append([])
            for j in line:
                seznam[pos].append(j.strip())

            for zadni in range(3):
                zadni += 1
                if seznam[pos][zadni] != nullString:
                    seznam[pos][zadni] = seznam[pos][zadni][1:-1].split(separator2)
                    for i, j in enumerate(seznam[pos][zadni]):
                        seznam[pos][zadni][i] = j.strip().strip("'")

    if debug:
        print(
            "\nAllMusic awards info:\n[imebenda, albumAwards, songAwards, grammys]\n")
        for i in seznam:
            print(i)

    print("Importing done!")

    return seznam

#[imeBenda, imeAlbuma, linkSlikeAlbuma, releaseDate, casAlbuma, recordingDate]
def readFilesAllMusicAlbums(debug=False):
    print("\nImporting Allmusic albums...")
    seznam = []
    with open(bendiAllMusicAlbumsFile, 'r', encoding='utf-8') as readFrom:
        for pos, line in enumerate(readFrom):
            line = line.split(separator)

            seznam.append([])
            for j in line:
                if j != '\n':
                    seznam[pos].append(j.strip())

    if debug:
        print("\nAllMusic AlbumInfo:\n[imeBenda, imeAlbuma, linkSlikeAlbuma, releaseDate, casAlbuma, genre, styles(seznam), recordingDate]\n")
        for i in seznam:
            print(i)

    print("Importing done!")

    return seznam

#[imeBenda, ImeAlbuma, #, imePesmi, cas]
def readFilesAllMusicSongs(debug=True):
    print("\nImporting Allmusic songs...")
    seznam = []
    with open(bendiAllMusicSongsFile, 'r', encoding='utf-8') as readFrom:
        for pos, line in enumerate(readFrom):
            line = line.split(separator)
            seznam.append([])
            for j in line:
                if j != '\n':
                    seznam[pos].append(j.strip().strip("'").strip("\n"))

    if debug:
        print("\nAllMusic AlbumInfo:\n[imeBenda, ImeAlbuma, #, imePesmi, cas]\n")
        for i in seznam:
            print(i)

    print("Importing done!")

    return seznam

# bendiIme = readFilesWiki(True)
# bendiAllMusicLink = readFilesLinks(True)
# bendiAllMusicSidebar = readFilesAllMusicSidebar(False)
# bendiALlMusicAwards = readFilesAllMusicAwards(True)
# bendiAllMusicAlbums = readFilesAllMusicAlbums(True)
# bendiAllMusicSongs = readFilesAllMusicSongs(True)



